package store.developer.seop.api.member.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.member.enums.Gender;
import store.developer.seop.api.member.model.MemberRequest;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "닉네임")
    @Column(nullable = false, length = 20)
    private String nickName;
    @ApiModelProperty(notes = "취미")
    @Column(nullable = false, length = 50)
    private String hobby;
    @ApiModelProperty(notes = "성별")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 5)
    private Gender gender;
    @ApiModelProperty(notes = "x좌표")
    @Column(nullable = false)
    private Double posX;
    @ApiModelProperty(notes = "y좌표")
    @Column(nullable = false)
    private Double posY;

    private Member(MemberBuilder builder) {
        this.nickName = builder.nickName;
        this.hobby = builder.hobby;
        this.gender = builder.gender;
        this.posX = builder.posX;
        this.posY = builder.posY;
    }
    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String nickName;
        private final String hobby;
        private final Gender gender;
        private final Double posX;
        private final Double posY;

        public MemberBuilder(MemberRequest request) {
            this.nickName = request.getNickName();
            this.hobby = request.getHobby();
            this.gender = request.getGender();
            this.posX = request.getPosX();
            this.posY = request.getPosY();
        }
        @Override
        public Member build() {
            return new Member(this);
        }
    }
}

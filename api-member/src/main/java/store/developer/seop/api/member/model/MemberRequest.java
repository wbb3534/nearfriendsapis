package store.developer.seop.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import store.developer.seop.api.member.enums.Gender;

@Getter
@Setter
public class MemberRequest {
    @ApiModelProperty(notes = "닉네임(~20글자)", required = true)
    private String nickName;
    @ApiModelProperty(notes = "취미(~50글자)", required = true)
    private String hobby;
    @ApiModelProperty(notes = "성별")
    private Gender gender;
    @ApiModelProperty(notes = "x좌표", required = true)
    private Double posX;
    @ApiModelProperty(notes = "y좌표", required = true)
    private Double posY;
}

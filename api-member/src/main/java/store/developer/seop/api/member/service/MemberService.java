package store.developer.seop.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.member.entity.Member;
import store.developer.seop.api.member.model.MemberRequest;
import store.developer.seop.api.member.model.NearFriendsItem;
import store.developer.seop.api.member.repository.MemberRepository;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ListConvertService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    @PersistenceContext
    EntityManager entityManager;

    public void setMember(MemberRequest request) {
        Member addData = new Member.MemberBuilder(request).build();

        memberRepository.save(addData);
    }

    /**
     * 근처 친구 리스트를 가져온다.
     *
     * @param posX ex 126.머시기....
     * @param posY ex 37.머시기....
     * @param distance 몇km의 친구 리스트를 가져올지.. ex 1, 2, 3
     * @return
     */

    public ListResult<NearFriendsItem> getNearFriends(double posX, double posY, int distance) {
        double distanceResult = distance * 1000; // 미터기준으로 변경..

        String queryString = "select * from public.get_near_friends("+ posX +"," + posY + ", " + distanceResult + ")";
        Query nativeQuery = entityManager.createNativeQuery(queryString); // 이러면 디비에서 값을 가져 왔겟지?

        List<Object[]> resultList = nativeQuery.getResultList();

        List<NearFriendsItem> result = new ArrayList<>();

        for (Object[] item : resultList) {
            result.add(
                    new NearFriendsItem.NearFriendsItemBuilder(
                            item[0].toString(),
                            item[1].toString(),
                            item[2].toString(),
                            Double.parseDouble(item[3].toString())).build()
            );
        }
        return ListConvertService.settingResult(result);
    }
}

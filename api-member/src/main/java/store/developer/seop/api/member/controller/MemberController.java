package store.developer.seop.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.member.model.MemberRequest;
import store.developer.seop.api.member.model.NearFriendsItem;
import store.developer.seop.api.member.model.NearFriendsSearchRequest;
import store.developer.seop.api.member.service.MemberService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "회원 등록")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "주변친구 검색")
    @PostMapping("/search/near/friend") // 기능은 R(Get)이지만 body를 쓰기 위해 PostMapping 으로 정의함
    public ListResult<NearFriendsItem> getSearchNearFriend(@RequestBody @Valid NearFriendsSearchRequest request) { // PostMapping 이기때문에 body로 값 받기 가능

        return ResponseService.getListResult(memberService.getNearFriends(request.getPosX(), request.getPosY(), request.getDistance()), true );
    }
}

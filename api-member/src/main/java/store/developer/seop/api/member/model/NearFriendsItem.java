package store.developer.seop.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.member.enums.Gender;
import store.developer.seop.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NearFriendsItem {
    @ApiModelProperty(notes = "닉네임")
    private String nickname;
    @ApiModelProperty(notes = "취미")
    private String hobby;
    @ApiModelProperty(notes = "성별")
    private String gender;
    @ApiModelProperty(notes = "성별 이름")
    private String genderName;
    @ApiModelProperty(notes = "자신과 떨어진 거리")
    private Double distanceM;

    private NearFriendsItem(NearFriendsItemBuilder builder) {
        this.nickname = builder.nickname;
        this.hobby = builder.hobby;
        this.gender = builder.gender;
        this.genderName = builder.genderName;
        this.distanceM = builder.distanceM;
    }
    public static class NearFriendsItemBuilder implements CommonModelBuilder<NearFriendsItem> {
        private final String nickname;
        private final String hobby;
        private final String gender;
        private final String genderName;
        private final Double distanceM;

        public NearFriendsItemBuilder(String nickname, String hobby, String gender, Double distanceM) {
            this.nickname = nickname;
            this.hobby = hobby;
            this.gender = gender;
            this.genderName = Gender.valueOf(gender).toString();
            this.distanceM = distanceM;
        }
        @Override
        public NearFriendsItem build() {
            return new NearFriendsItem(this);
        }
    }
}

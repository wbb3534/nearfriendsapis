package store.developer.seop.api.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import store.developer.seop.api.member.entity.Member;
import store.developer.seop.api.member.enums.Gender;

import java.util.List;



public interface MemberRepository extends JpaRepository<Member, Long> {
    // 네거티브 커리로 함 변수처리할땐 : 을 붙힌다
    @Query(
            value = "select * from member where earth_distance(ll_to_earth(posy, posx), ll_to_earth(:pointY, :pointX)) < :searchDistance",
            nativeQuery = true

    )
    // 위에 변수처리한 것들을 바구니에담고 인식을 하게 하기 위해 @Param을 붙힌다.
    List<Member> findAllByNearFriends(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") int searchDistance);
    @Query(
            value = "select * from member where earth_distance(ll_to_earth(posy, posx), ll_to_earth(:pointY, :pointX)) < :searchDistance and gender = :gender",
            nativeQuery = true
    )
    List<Member> findAllByNearFriendsByGender(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") int searchDistance, @Param("gender") String gender);
}

package store.developer.seop.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NearFriendsSearchRequest {
    @ApiModelProperty(notes = "x좌표")
    private Double posX;
    @ApiModelProperty(notes = "y좌표")
    private Double posY;
    @ApiModelProperty(notes = "주변거리")
    private Integer distance;
}

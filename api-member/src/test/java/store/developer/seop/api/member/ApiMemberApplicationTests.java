package store.developer.seop.api.member;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import store.developer.seop.api.member.entity.Member;
import store.developer.seop.api.member.enums.Gender;
import store.developer.seop.api.member.repository.MemberRepository;

import java.util.List;

@SpringBootTest
public class ApiMemberApplicationTests {
    @Autowired
    MemberRepository memberRepository;
    @Test
    void contextLoads() {

    }
    @Test
    void queryTest() {
        double pointY = 37.3179417691029;
        double pointX = 126.835639929462;
        int searchDistance = 5000;

        List<Member> result = memberRepository.findAllByNearFriends(pointY, pointX, searchDistance);

        int a = 0;

    }
    @Test
    void nearFriendsByGender() {
        double pointY = 37.3179417691029;
        double pointX = 126.835639929462;
        int searchDistance = 5000;
        String gender = Gender.MAN.toString();

        List<Member> result = memberRepository.findAllByNearFriendsByGender(pointY, pointX, searchDistance, gender);

        int a = 0;
    }
}

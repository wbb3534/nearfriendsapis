package store.developer.seop.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SearchAddressResponse {
    @ApiModelProperty(notes = "문서아이템 그릇")
    private List<SearchAddressDocumentsItem> documents;
}

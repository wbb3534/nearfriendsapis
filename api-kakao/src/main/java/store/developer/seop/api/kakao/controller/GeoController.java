package store.developer.seop.api.kakao.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import store.developer.seop.api.kakao.model.Code2RegionCodeResponse;
import store.developer.seop.api.kakao.model.SearchAddressResponse;
import store.developer.seop.api.kakao.service.GeoService;
import store.developer.seop.common.response.model.SingleResult;
import store.developer.seop.common.response.service.ResponseService;

@Api(tags = "지도 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/geo")
public class GeoController {
    private final GeoService geoService;

    @ApiOperation(value = "주소검색해서 좌표얻기")
    @GetMapping("/search/address")
    public SingleResult<SearchAddressResponse> getSearchAddress(@RequestParam("searchAddress") String searchAddress) {
        return ResponseService.getSingleResult(geoService.getAddress(searchAddress));
    }
    @ApiOperation(value = "좌표로 행정구정보 받기")
    @GetMapping("/search/Administrative-zone")
    public SingleResult<Code2RegionCodeResponse> getAdministrativeZone(@RequestParam("xValue") String xValue, @RequestParam("yValue") String yValue ) {
        return ResponseService.getSingleResult(geoService.getAdministrativeZone(xValue, yValue));
    }

}

package store.developer.seop.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cord2RegionCodeDocumentsItem {
    @ApiModelProperty(notes = "H(행정동) 또는 B(법정동)")
    private String region_type;
    @ApiModelProperty(notes = "전체 지역 명칭")
    private String address_name;
    @ApiModelProperty(notes = "지역 1Depth, 시도 단위")
    private String region_1depth_name;
    @ApiModelProperty(notes = "지역 2Depth, 구 단위")
    private String region_2depth_name;
    @ApiModelProperty(notes = "지역 3Depth, 동 단위")
    private String region_3depth_name;
    @ApiModelProperty(notes = "X 좌표값, 경위도인 경우 경도(longitude)")
    private String x;
    @ApiModelProperty(notes = "X 좌표값, 경위도인 경우 경도(longitude)")
    private String y;
}

package store.developer.seop.api.kakao.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import store.developer.seop.api.kakao.enums.KakaoUri;
import store.developer.seop.api.kakao.lib.RestApi;
import store.developer.seop.api.kakao.model.Code2AddressResponse;
import store.developer.seop.api.kakao.model.Code2RegionCodeResponse;
import store.developer.seop.api.kakao.model.SearchAddressResponse;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Service
public class GeoService {
    @Value("${kakao.api.domain}")
    String KAKAO_API_DOMAIN;

    @Value("${kakao.api.rest-key}")
    String KAKAO_API_REST_KEY;

    public SearchAddressResponse getAddress(String addressValue) {
        String apiFullUri = KAKAO_API_DOMAIN + KakaoUri.SEARCH_ADDRESS.getApiSubUrl();

        String queryString = "?query=" + URLEncoder.encode(addressValue, StandardCharsets.UTF_8);

        String url = apiFullUri + queryString;

        return RestApi.callApi(HttpMethod.GET, url, KAKAO_API_REST_KEY, null, SearchAddressResponse.class);

        /*// Uri 라는 객체이다.
        URI url = URI.create(apiFullUri + queryString);

        // 자바에서 다른서버에 전화를 하려면 RestTemplate사용 프론트에 axios같은 존재
        RestTemplate restTemplate = new RestTemplate();
        // 헤더에 키 등을 추가하기 위해 만듬
        HttpHeaders headers = new HttpHeaders();
        // 헤더에다가 키 값을 추가
        headers.add("Authorization", "KakaoAK " + KAKAO_API_REST_KEY);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        // 한글이 깨지는걸 방지하기 위해 ;charset=UTF-8 추가 api콜할 때 추가안하면 안됨
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        // 외부 api에 요청  준비된 url에 겟맵핑으로 요청 (헤드값을 넣어서)
        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, url);
        // 요청하고 나온 결과값을 받는다
        ResponseEntity<SearchAddressResponse> responseEntity = restTemplate.exchange(requestEntity, SearchAddressResponse.class);

        return responseEntity.getBody();*/
    }

    public Code2RegionCodeResponse getAdministrativeZone(String xValue, String yValue) {
        String apiFullDomain = KAKAO_API_DOMAIN + KakaoUri.CORD2REGIONCODE.getApiSubUrl();

        String queryString = "?x=" + xValue + "&y=" + yValue;

        String url = apiFullDomain + queryString;


        return RestApi.callApi(HttpMethod.GET, url, KAKAO_API_REST_KEY, null, Code2RegionCodeResponse.class);
    }

    //public Code2AddressResponse getConvertCoordinatesToAddress(String xValue, String yValue, String codeValue) {

    //}

}

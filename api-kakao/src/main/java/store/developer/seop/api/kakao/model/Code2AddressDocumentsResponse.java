package store.developer.seop.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Code2AddressDocumentsResponse {
    @ApiModelProperty(notes = "지역 1Depth, 시도 단위")
    private String region_1depth_name;
    @ApiModelProperty(notes = "지역 2Depth, 구 단위")
    private String region_2depth_name;
    @ApiModelProperty(notes = "지역 3Depth, 동 단위")
    private String region_3depth_name;
    @ApiModelProperty(notes = "도로명")
    private String road_name;
    @ApiModelProperty(notes = "건물 본번")
    private String main_building_no;
    @ApiModelProperty(notes = "건물 부번, 없을 경우 빈 문자열(\"\") 반환")
    private String sub_building_no;
    @ApiModelProperty(notes = "건물 이름")
    private String building_name;
    @ApiModelProperty(notes = "우편번호(5자리)")
    private String zone_no;
}

package store.developer.seop.api.kakao.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum KakaoUri {
    SEARCH_ADDRESS("주소검색", "/v2/local/search/address.json")
    , CORD2REGIONCODE("좌표로 행정구역 정보 받기", "/v2/local/geo/coord2regioncode.json")
    , CORD2ADDRESS("좌표 주소 변환하기", "/v2/local/geo/coord2address.json")
    ;
    private final String apiName;
    private final String apiSubUrl;
}

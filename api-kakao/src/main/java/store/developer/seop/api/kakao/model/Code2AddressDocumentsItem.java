package store.developer.seop.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Code2AddressDocumentsItem {
    private Code2AddressDocumentsResponse road_address;
}

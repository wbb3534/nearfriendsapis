package store.developer.seop.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Code2AddressResponse {
    private List<Code2AddressDocumentsItem> documents;
}

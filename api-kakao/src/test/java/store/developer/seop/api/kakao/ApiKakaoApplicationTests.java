package store.developer.seop.api.kakao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import store.developer.seop.api.kakao.enums.KakaoUri;
import store.developer.seop.api.kakao.model.Code2AddressResponse;
import store.developer.seop.api.kakao.model.Code2RegionCodeResponse;
import store.developer.seop.api.kakao.model.SearchAddressResponse;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@SpringBootTest
public class ApiKakaoApplicationTests {
    @Test
    void contextLoads() {

    }

    @Test
    void apiCallTest() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_ADDRESS.getApiSubUrl();

        String addressValue = "고잔로 88";
        // 한글을 못읽을 수 있기 때문에 한글을 국제표준으로 인코딩 시킨다
        String queryString = "?query=" + URLEncoder.encode(addressValue, StandardCharsets.UTF_8);

        // Uri 라는 객체이다.
        URI url = URI.create(apiFullUri + queryString);

        // 자바에서 다른서버에 전화를 하려면 RestTemplate사용 프론트에 axios같은 존재
        RestTemplate restTemplate = new RestTemplate();
        // 헤더에 키 등을 추가하기 위해 만듬
        HttpHeaders headers = new HttpHeaders();
        // 헤더에다가 키 값을 추가
        headers.add("Authorization", "KakaoAK 7a4ab6a8847f5bc3b407757d36933031");
        // 응답결과를 json으로 달라고 명시 함
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        // 한글이 깨지는걸 방지하기 위해 ;charset=UTF-8 추가 api콜할 때 추가안하면 안됨
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        // 외부 api에 요청  준비된 url에 겟맵핑으로 요청 (헤드값을 넣어서)
        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, url);
        // 요청하고 나온 결과값을 받는다
        ResponseEntity<SearchAddressResponse> responseEntity = restTemplate.exchange(requestEntity, SearchAddressResponse.class);

        SearchAddressResponse result = responseEntity.getBody();

        int a = 1;
    }
    @Test
    void code2RegionCodeApiTest() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullDomain = apiDomain + KakaoUri.CORD2REGIONCODE.getApiSubUrl();

        String xValue = "127.1086228";
        String yValue = "37.4012191";

        //double xValue = 127.1086228;
        //double yValue = 37.4012191;

        String queryString = "?x=" + xValue + "&y=" + yValue;

        //String queryString = "?x=" + xValue + "&y="
        //        + yValue;

        URI url = URI.create(apiFullDomain + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        // 헤더에다가 키 값을 추가
        headers.add("Authorization", "KakaoAK 7a4ab6a8847f5bc3b407757d36933031");
        // 응답결과를 json으로 달라고 명시 함
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        // 한글이 깨지는걸 방지하기 위해 ;charset=UTF-8 추가 api콜할 때 추가안하면 안됨
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, url);
        ResponseEntity<Code2RegionCodeResponse> responseEntity = restTemplate.exchange(requestEntity, Code2RegionCodeResponse.class);

        Code2RegionCodeResponse result = responseEntity.getBody();

        int a = 1;
    }
    @Test
    void code2Address() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullDomain = apiDomain + KakaoUri.CORD2ADDRESS.getApiSubUrl();

        String xValue = "127.423084873712";
        String yValue = "37.0789561558879";
        String codeValue = "WGS84";

        String queryString = "?x=" + xValue + "&y=" + yValue + "&input_coord=" + codeValue;

        URI url = URI.create(apiFullDomain + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        // 헤더에다가 키 값을 추가
        headers.add("Authorization", "KakaoAK 7a4ab6a8847f5bc3b407757d36933031");
        // 응답결과를 json으로 달라고 명시 함
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        // 한글이 깨지는걸 방지하기 위해 ;charset=UTF-8 추가 api콜할 때 추가안하면 안됨
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, url);
        ResponseEntity<Code2AddressResponse> responseEntity = restTemplate.exchange(requestEntity, Code2AddressResponse.class);

        Code2AddressResponse result = responseEntity.getBody();

        int a = 1;
    }

}

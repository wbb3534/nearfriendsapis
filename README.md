# 동네 친구 찾기 API

###  카카오 API를 이용하여 GPS(위도, 경도) 기술을 사용하여 동네친구를 찾을 수 있게 하는 서비스 API입니다.

### 제작기간
2023-03-06 ~ 2023-03-10
---

### 사용 기술
<pre>
- java 17
- JPA
- Postgres(*procedure)
</pre>
---
### 카카오 API 
* 주소 검색하기 API 사용
 >  주소  >  좌표로 변환
> 
 > 문서 - https://developers.kakao.com/docs/latest/ko/local/dev-guide
---
필요 프로시저
<pre>
CREATE OR REPLACE FUNCTION public.get_near_friends(positionX DOUBLE PRECISION, positionY DOUBLE PRECISION, distance DOUBLE PRECISION)  
RETURNS TABLE  
(  
    nickname varchar  
    , hobby varchar  
    , gender varchar  
    , distance_m double precision  
)  
as  
$$  
DECLARE  
    v_record RECORD;  
BEGIN  
    for v_record in (  
        select  
            member.nickname, member.hobby, member.gender, earth_distance(ll_to_earth(member.posy, member.posx), ll_to_earth(positionY, positionX)) as distance_m  
    from member  
    where earth_distance(ll_to_earth(member.posy, member.posx), ll_to_earth(positionY, positionX)) <= distance  
    order by distance_m asc  
    )  
    loop  
        nickname := v_record.nickname;  
        hobby := v_record.hobby;  
        gender := v_record.gender;  
        distance_m := v_record.distance_m;  
        return next;  
    end loop;  
END;  
$$  
LANGUAGE plpgsql 
</pre>
